﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(MVCApplicationForGIT2.Startup))]

namespace MVCApplicationForGIT2
{
	public partial class Startup
	{
		public void Configuration(IAppBuilder app) {
			ConfigureAuth(app);
		}
	}
}
